def factors(number):

    fact_list = []

    for i in range(2, number):
        if number % i == 0:
            fact_list.append(i)

    if not fact_list:
        number= str(number)
        m = " is a prime number"
        return number + m
        
    else:
        return fact_list

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
